#!usr/bin/python3
import numpy as np
from matplotlib import pyplot as plt
from world import Person, Success
from mplplot import 2dplot, 2dplotjson


class RoadToSuccess(Success):
    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
     
    def possibility(self, effort)
        return effort / (self.obstacle + effort)
        
    def possibility_curve(self, continuous_efforts, return_type='figure'):
        x = np.array(continuous_efforts)
        y = x / (self.obstacle + x)
        if return_type == 'figure':
            return 2dplot(x, y)
        else:
            return 2dplotjson(x, y)
