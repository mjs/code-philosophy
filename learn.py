#!usr/bin/python3
import numpy as np
from matplotlib import pyplot as plt
from world import Person, Knowledge


__all__ = ['Student', 'LearnKnowledge']
        
            
class Student(Person):
    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        
        
class LearnKnowledge(Knowledge):
    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
    
    def learning_curve(self, student, effort):
        # this is basically a logistic function
        effort = np.array(effort)
        known = self.knowledge / (1 + np.exp(-student.smartness * (effort - self.difficulty)))
        img = plt.figure()
        ax = img.add_subplot(111)
        ax.plot(effort, known)
        return img
        
if __name__ == '__main__':
    coding = LearnKnowledge(difficulty=10)
    jack = Student(smartness = 1, experience = 1)
    jacks_efforts = [1, 2, 3, 3, 3, 4, 4, 5, 5, 6, 7, 10]
    img = coding.learning_curve(jack, jacks_efforts)
    plt.show()
