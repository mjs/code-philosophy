class Person:
    def __init__(self, smartness=1, experience=1):
        self.smartness = smartness
        self.experience = experience
        
class Knowledge:
    def __init__(self, difficulty=5):
        self.knowledge = 1
        self.difficulty = difficulty 
        
class Success:
    def __init__(self, obstacle=50):
        self.obstacle = obstacle         
